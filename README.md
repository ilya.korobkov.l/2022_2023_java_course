## Описание

Здравствуйте!
Меня зовут Коробков Илья Леонидович. В этом репозитории я выкладываю решение домашних заданий по курсу "Введение в разработку корпоративных приложений на Java".



Ссылки на репозитории c решениями домашних заданий:

branch homework1

https://gitlab.com/ilya.korobkov.l/2022_2023_java_course/-/tree/homework1



branch homework2

https://gitlab.com/ilya.korobkov.l/2022_2023_java_course/-/tree/homework2



branch homework3

https://gitlab.com/ilya.korobkov.l/2022_2023_java_course/-/tree/homework3



branch homework4

https://gitlab.com/ilya.korobkov.l/2022_2023_java_course/-/tree/homework4



branch homework5

https://gitlab.com/ilya.korobkov.l/2022_2023_java_course/-/tree/homework5



branch homework6

https://gitlab.com/ilya.korobkov.l/2022_2023_java_course/-/tree/homework6



branch homework7 (промежуточная аттестация 1. Java Core)

https://gitlab.com/ilya.korobkov.l/2022_2023_java_course/-/tree/homework7



branch homework8 (промежуточная аттестация 2. Hibernate. PostgressSQL)

https://gitlab.com/ilya.korobkov.l/2022_2023_java_course/-/tree/homework8_sql_hibernate_



branch homework9 (Итоговая аттестация)

https://gitlab.com/ilya.korobkov.l/2022_2023_java_course/-/tree/homework9

## Контакты 

- группа 4, период обучения с 14 ноября 2022 по 03 апреля 2023
- email: ilya.korobkov.l@gmail.com

- telegram: @izorroman

